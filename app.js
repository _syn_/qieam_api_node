const express  = require('express');
const cors = require('cors');
const app = express();
app.use(cors());

const friends =[{name:"denis"},{name:"ah"}];

// friend {name:'friend name'}
// get("api/friends",=>return a list of friends )
app.get('/api/friends',(req,res)=>res.send({friends})); 

//post ("api/friend?name = xxx" => create  a new friend in query param)
app.post('/api/friend',(req,res) => 
{
	let name = req.query.name;
	console.log(req.query.name);
	friends.push({name});
	res.sendStatus(201);
});

//delete("api/friend?name = xxx" => delete a friend in friends)
app.delete('/api/friend',(req,res)=>
{
	const hasSameName = (friend)=>friend.name === req.query.name;
	let idxFriend = friends.findIndex(hasSameName);
	friends.splice(idxFriend,1);	
	res.sendStatus(202);
});

module.exports = app;